package com.example.planque.notegps;


public class Note {

    private int id;
    public String titre;
    public String texte;
    public String date;
    public double lat;
    public double lon;

    public Note(){}

    public Note(String titre, String texte, String date, double lat, double lon){
        this.titre=titre;
        this.texte=texte;
        this.date=date;
        this.lat=lat;
        this.lon=lon;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id=id;
    }

    public String getTitre(){
        return this.titre;
    }

    public void setTitre(String titre){
        this.titre=titre;
    }

    public String getTexte(){
        return this.texte;
    }

    public void setTexte(String texte){
        this.texte=texte;
    }

    public String getDate(){
        return this.date;
    }

    public void setDate(String date){
        this.date=date;
    }

    public double getLat(){
        return this.lat;
    }

    public void setLat(double lat){
        this.lat=lat;
    }

    public double getLon(){
        return this.lon;
    }

    public void setLon(double lon){
        this.lon=lon;
    }

    public String toString(){
        return "id: "+this.id+" titre: "+this.titre+" texte: "+this.texte+" latitude: "+this.lat+" longitude: "+this.lon+" date: "+this.date;
    }
}
