package com.example.planque.notegps;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class NoteBDD {

    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "note.db";

    private static final String TABLE_NOTE = "table_notes";
    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;
    private static final String COL_TITRE = "TITRE";
    private static final int NUM_COL_TITRE = 1;
    private static final String COL_TEXT = "TEXT";
    private static final int NUM_COL_TEXT = 2;
    private static final String COL_DATE = "DATE";
    private static final int NUM_COL_DATE = 3;
    private static final String COL_LAT = "LATITUDE";
    private static final int NUM_COL_LAT = 4;
    private static final String COL_LON = "LONGITUDE";
    private static final int NUM_COL_LON = 5;

    private SQLiteDatabase bdd;

    private MaBaseSQLite maBaseSQLite;

    public NoteBDD(Context context){
        //On créer la BDD et sa table
        maBaseSQLite = new MaBaseSQLite(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open(){
        //on ouvre la BDD en écriture
        bdd = maBaseSQLite.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }

    public long insertNote(Note note){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(COL_TITRE, note.getTitre());
        values.put(COL_TEXT, note.getTexte());
        values.put(COL_DATE, note.getDate());
        values.put(COL_LAT, note.getLat());
        values.put(COL_LON, note.getLon());
        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_NOTE, null, values);
    }

    public int updateNote(int id, Note note){
        //La mise à jour d'un livre dans la BDD fonctionne plus ou moins comme une insertion
        //il faut simple préciser quelle livre on doit mettre à jour grâce à l'ID
        ContentValues values = new ContentValues();
        values.put(COL_TITRE, note.getTitre());
        values.put(COL_TEXT, note.getTexte());
        values.put(COL_DATE, note.getDate());
        values.put(COL_LAT, note.getLat());
        values.put(COL_LON, note.getLon());
        return bdd.update(TABLE_NOTE, values, COL_ID + " = " +id, null);
    }

    public int removeNoteWithID(int id){
        //Suppression d'un livre de la BDD grâce à l'ID
        return bdd.delete(TABLE_NOTE, COL_ID + " = " +id, null);
    }


    public Note getNoteWithTitre(String titre){
        //Récupère dans un Cursor les valeur correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        Cursor c = bdd.query(TABLE_NOTE, new String[] {COL_ID, COL_TITRE, COL_TEXT, COL_DATE, COL_LAT, COL_LON}, COL_TITRE + " LIKE \"" + titre +"\"", null, null, null, null);
        return cursorToNote(c);
    }

    public List<Note> getNotes(){
        List<Note> notes = new ArrayList<Note>();
        String[] colonnes = {COL_ID, COL_TITRE, COL_TEXT, COL_DATE, COL_LAT, COL_LON};

        Cursor c = bdd.query(TABLE_NOTE, colonnes, null, null, null, null, null);
        c.moveToFirst();
        while(!c.isAfterLast()){
            Note note = new Note();
            note.setId(c.getInt(NUM_COL_ID));
            note.setTitre(c.getString(NUM_COL_TITRE));
            note.setTexte(c.getString(NUM_COL_TEXT));
            note.setDate(c.getString(NUM_COL_DATE));
            note.setLat(c.getDouble(NUM_COL_LAT));
            note.setLon(c.getDouble(NUM_COL_LON));
            notes.add(note);
            c.moveToNext();
        }
        c.close();
        return notes;
    }

    //Cette méthode permet de convertir un cursor en un livre
    private Note cursorToNote(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //On créé un livre
        Note note = new Note();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        note.setId(c.getInt(NUM_COL_ID));
        note.setTitre(c.getString(NUM_COL_TITRE));
        note.setTexte(c.getString(NUM_COL_TEXT));
        note.setDate(c.getString(NUM_COL_DATE));
        note.setLat(c.getDouble(NUM_COL_LAT));
        note.setLon(c.getDouble(NUM_COL_LON));
        //On ferme le cursor
        c.close();
        //On retourne le livre
        return note;
    }
}