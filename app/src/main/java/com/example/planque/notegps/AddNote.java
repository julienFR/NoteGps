package com.example.planque.notegps;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.EditText;

public class AddNote extends Activity {

    LocationManager lm;
    Location location;
    double longitude;
    double latitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        //Récupération coordonnées
        lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        try {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }
        catch (Exception e){
            longitude = 0.0;
            latitude = 0.0;
        }

        Intent intent = getIntent();



        final EditText titre = (EditText) findViewById(R.id.editText);
        final EditText text = (EditText) findViewById(R.id.edit);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( titre.getText().toString().trim().equals("")){
                    titre.setError( "Titre obligatoire !" );
                }
                else {
                    saveNote(titre.getText().toString(), text.getText().toString(), latitude, longitude);
                }
            }
        });
    }

    public void saveNote(String titre, String texte, double latitude, double longitude){
        Intent previousMainActivity = new Intent(this, MainActivity.class);
        previousMainActivity.putExtra("titre",titre);
        previousMainActivity.putExtra("texte",texte);
        previousMainActivity.putExtra("latitude",latitude);
        previousMainActivity.putExtra("longitude",longitude);
        setResult(RESULT_OK,previousMainActivity);
        finish();
    }
}
