package com.example.planque.notegps;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    List<android.text.Spanned> titres;
    List<Note> notes;
    ArrayAdapter<android.text.Spanned> adapter;
    NoteBDD bdd;
    List<Note> notesFromBdd;

    LocationManager lm;
    Location location;
    double longitude;
    double latitude;

    ConnectivityManager cm;
    NetworkInfo activeNetwork;
    boolean isConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialisation des valeurs pour la ListView
        titres = new ArrayList<android.text.Spanned>();
        notes = new ArrayList<Note>();

        //Initialisation de la BDD
        bdd = new NoteBDD(this);

        cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        //Récupération de la ListView
        ListView lv = (ListView) findViewById(R.id.listView);

        adapter = new ArrayAdapter<android.text.Spanned>(this, android.R.layout.simple_list_item_1, titres);

        refreshNotes(bdd);

        //Affichage des élements
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                modifierNote(notesFromBdd.get(position).getId(), notesFromBdd.get(position).getTitre(), notesFromBdd.get(position).getTexte(), getLatitude(), getLongitude());
            }
        });
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(MainActivity.this, "Vous avez cliquez longtemps à la position " + position+" avec l'id "+notes.get(position).getId(), Toast.LENGTH_SHORT).show();
                alertMessage(position);
                return true;
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addNote();
            }
        });
    }

    public void alertMessage(final int pos) {
        new AlertDialog.Builder(this)
                .setTitle("Suppression d'une note")
                .setMessage("Voulez-vous vraiment supprimer la note ?")
                .setIcon(android.R.drawable.ic_delete)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        supprimerNote(bdd, pos);
                    }
                })
                .setNegativeButton(R.string.no, null).show();
    }

    public void supprimerNote(NoteBDD bdd, int pos) {
        titres.remove(pos);
        bdd.open();
        bdd.removeNoteWithID(notes.get(pos).getId());
        bdd.close();
        notes.remove(pos);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addNote() {
        Intent addNote = new Intent(this, AddNote.class);
        startActivityForResult(addNote, 1);
    }

    public void modifierNote(int id, String titre, String texte, double latitude, double longitude) {
        Intent modifierNote = new Intent(this, ModifierNote.class);
        modifierNote.putExtra("id", id);
        modifierNote.putExtra("titre", titre);
        modifierNote.putExtra("texte", texte);
        modifierNote.putExtra("latitude", latitude);
        modifierNote.putExtra("longitude", longitude);
        startActivityForResult(modifierNote, 2);
    }

    public void refreshNotes(NoteBDD bdd) {
        //Récupération de la ListView
        ListView lv = (ListView) findViewById(R.id.listView);

        titres.clear();
        notes.clear();
        bdd.open();
        notesFromBdd = bdd.getNotes();

        String noteText;
        String addr;

        if(notesFromBdd!=null){
            for(int i = 0; i < notesFromBdd.size();i++){
                try{
                    noteText = notesFromBdd.get(i).getTexte().substring(0,100)+" ... ";
                }
                catch (Exception e){
                    noteText = notesFromBdd.get(i).getTexte();
                }
                try{
                    if(isConnected) {
                        addr = getAddress(notesFromBdd.get(i).getLat(), notesFromBdd.get(i).getLon());
                    }
                    else{
                        addr = "Requiert Internet";
                    }
                }
                catch(Exception e){
                    addr = "Position gps inconnue";
                }
                if(addr.equals("")){
                    addr = "Position gps inconnue";
                }
                titres.add(Html.fromHtml("<h1>"+notesFromBdd.get(i).getTitre()+"</h1><p>"+noteText+"</p><font color=\"gray\">Adresse : "+addr+"<br>Modifié le "+notesFromBdd.get(i).getDate()+"</font><br>"));
                notes.add(notesFromBdd.get(i));
            }
        }
        adapter.notifyDataSetChanged();
        lv.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String titre = data.getStringExtra("titre");
                String texte = data.getStringExtra("texte");
                double longitude = getLongitude();
                double latitude = getLatitude();
                String date = getDate();
                ajouterNoteBDD(titre, texte, date, latitude, longitude);
            }
        }
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                String titre = data.getStringExtra("titre");
                String texte = data.getStringExtra("texte");
                int id = data.getIntExtra("idNote", -1);
                System.out.println(titre+"\n"+texte+"\n"+id);
                double longitude = getLongitude();
                double latitude = getLatitude();
                String date = getDate();
                Note note = new Note(titre, texte, date, latitude, longitude);
                modifierNoteBDD(id,note);
            }
        }
    }

    private double getLongitude() {
        lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        try {
            longitude = location.getLongitude();
        }
        catch (Exception e){
            longitude = 0.0;
        }
        return longitude;
    }

    private double getLatitude() {
        lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        try {
            latitude = location.getLatitude();
        }
        catch (Exception e){
            latitude = 0.0;
        }
        return latitude;
    }


    public String getDate() {
        Calendar rightNow = Calendar.getInstance();
        return rightNow.get(Calendar.DAY_OF_MONTH) + "/" + rightNow.get(Calendar.MONTH) + "/" + rightNow.get(Calendar.YEAR) + " à " + rightNow.get(Calendar.HOUR_OF_DAY) + "h" + rightNow.get(Calendar.MINUTE);
    }

    public void ajouterNoteBDD(String titre, String texte, String date, double latitude, double longitude) {
        Note note = new Note(titre, texte, date, latitude, longitude);
        bdd.open();
        bdd.insertNote(note);
        refreshNotes(bdd);
        bdd.close();
    }

    public void modifierNoteBDD(int id, Note note) {
        bdd.open();
        bdd.updateNote(id,note);
        refreshNotes(bdd);
        bdd.close();
    }

    private String getAddress(double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                result.append(address.getLocality()).append(", ");
                result.append(address.getCountryName());
            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }
        return result.toString();
    }
}
